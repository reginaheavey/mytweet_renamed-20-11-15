package app.mytweet;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Date;

import static app.mytweet.IntentHelper.navigateUp;

public class mytweetActivity extends Activity implements TextWatcher, CompoundButton.OnCheckedChangeListener
{
  private Button tweetButton;
  private EditText tweet;
  private TextView charCount;
  private TextView tweetDateTime;
  public Date date;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_mytweet);
    getActionBar().setDisplayHomeAsUpEnabled(true);

    tweetButton = (Button) findViewById(R.id.tweetButton);
    if (tweetButton != null)
    {
      Log.v("Tweet", "Really got the Tweet button");
    }

    tweet = (EditText) findViewById(R.id.tweet);
    tweet.addTextChangedListener(this);
    charCount = (TextView) findViewById(R.id.charCount);

    tweetDateTime = (TextView) findViewById(R.id.tweetDateTime);
    this.date = new Date();
    //tweetDateTime.setEnabled(true);
    tweetDateTime.setText(getDateString());
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_mytweet, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    switch (item.getItemId())
    {
      case android.R.id.home:
        navigateUp(this);
        return true;
    }

    return super.onOptionsItemSelected(item);

  }

  public void tweetButtonPressed(View view)
  {
    Toast toast = Toast.makeText(this, "Message Sent", Toast.LENGTH_SHORT);
    toast.show();
  }


  @Override
  public void beforeTextChanged(CharSequence s, int start, int count, int after)
  {

  }

  @Override
  public void onTextChanged(CharSequence s, int start, int before, int count)
  {
    int x = 140 - s.length();
    charCount.setText("" + x);
    if (x == 0)
    {
      //Toast toast = Toast.makeText(getActivity(), "Character Limit Reached", Toast.LENGTH_SHORT);
      Toast toast = Toast.makeText(this, "Character Limit Reached", Toast.LENGTH_SHORT);
      toast.show();

    }
  }

  @Override
  public void afterTextChanged(Editable s)
  {

  }

  public String getDateString()
  {
    return DateFormat.getDateTimeInstance().format(date);
  }

  @Override
  public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
  {

  }
}

