package app.mytweet;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Toast;

import static app.mytweet.IntentHelper.startActivityWithDataForResult;

public class Timeline extends Activity implements CompoundButton.OnCheckedChangeListener
{

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_timeline);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.timeline, menu);
    return true;
   }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
//    // Handle action bar item clicks here. The action bar will
//    // automatically handle clicks on the Home/Up button, so long
//    // as you specify a parent activity in AndroidManifest.xml.
//    int id = item.getItemId();
//
//    //noinspection SimplifiableIfStatement
//    if (id == R.id.action_settings)
//    {
//      return true;
//    }
//
//    return super.onOptionsItemSelected(item);
//  }
    switch (item.getItemId())
    {
      case R.id.action_settings:
        Toast toast1 = Toast.makeText(this, "Settings Selected", Toast.LENGTH_SHORT);
        toast1.show();
        break;

            case R.id.menuClear:
        Toast toast2 = Toast.makeText(this, "Clear Selected", Toast.LENGTH_SHORT);
        toast2.show();
        break;

      case R.id.menu_item_new_tweet:
        startActivityWithDataForResult(this, mytweetActivity.class, "myTweet_ID", 0, 0);
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
  {

  }
}
