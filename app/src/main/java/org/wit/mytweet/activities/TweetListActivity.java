package org.wit.mytweet.activities;

import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.R;
import org.wit.mytweet.models.Portfolio;
import org.wit.mytweet.models.Tweet;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;

import static org.wit.helpers.IntentHelper.startActivityWithData;

public class TweetListActivity extends Activity implements AdapterView.OnItemClickListener
{
  private ListView listView;
  private Portfolio portfolio;
  private TweetAdapter adapter;

  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    //setTitle(R.string.app_name);
    setContentView(R.layout.activity_tweetlist);

    listView = (ListView) findViewById(R.id.tweetList);
    listView.setOnItemClickListener(this);

    MyTweetApp app = (MyTweetApp) getApplication();
    portfolio = app.portfolio;

    adapter = new TweetAdapter(this, portfolio.tweets);
    listView.setAdapter(adapter);
  }

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id)
  {
    Tweet tweet = adapter.getItem(position);
    startActivityWithData(this, TweetActivity.class, "TWEET_ID", tweet.id);
  }

  @Override
  public void onResume()
  {
    super.onResume();
    adapter.notifyDataSetChanged();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    MenuInflater menuInflater = getMenuInflater();
    menuInflater.inflate(R.menu.tweetlist, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    switch (item.getItemId())
    {
      case R.id.menu_item_new_tweet:
        startActivity(new Intent(this, TweetActivity.class));
        break;

      case R.id.menuClear:
        adapter.clear();
        break;
    }
    return true;
  }
}

// Adapter Class
class TweetAdapter extends ArrayAdapter<Tweet>
{
  private Context context;

  public TweetAdapter(Context context, ArrayList<Tweet> tweets)
  {
    super(context, 0, tweets);
    this.context = context;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent)
  {
    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    if (convertView == null)
    {
      convertView = inflater.inflate(R.layout.list_item_tweet, null);
    }
    Tweet twe = getItem(position);

    TextView tweetText = (TextView) convertView.findViewById(R.id.tweet_list_item_tweetText);
    tweetText.setText(twe.tweetText);

    TextView tweetDateTime = (TextView) convertView.findViewById(R.id.tweet_list_item_dateTextView);
    tweetDateTime.setText(twe.getDateString());

    return convertView;
  }
}
