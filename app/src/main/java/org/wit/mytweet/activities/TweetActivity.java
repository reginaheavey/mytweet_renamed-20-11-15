package org.wit.mytweet.activities;


import java.util.UUID;

import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Portfolio;
import org.wit.mytweet.models.Tweet;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.text.Editable;
import android.text.TextWatcher;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;


import static org.wit.helpers.ContactHelper.getContact;
import static org.wit.helpers.ContactHelper.getEmail;
import static org.wit.helpers.IntentHelper.navigateUp;
import static org.wit.helpers.IntentHelper.selectContact;
import static org.wit.helpers.IntentHelper.sendEmail;
import static org.wit.helpers.IntentHelper.startActivityWithDataForResult;

public class TweetActivity extends Activity implements TextWatcher, View.OnClickListener
{
  private EditText tweetText;
  private Tweet tweet;

  private Button tweetButton;
  private TextView charCount;
  private TextView tweetDateTime;
  public List<Tweet> tweets = new ArrayList<Tweet>();

  private Portfolio portfolio;

  private static final int REQUEST_CONTACT = 1;
  private Button selectContactButton;
  private Button emailTweetButton;

@Override
protected void onCreate(Bundle savedInstanceState)
    {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.fragment_tweet);
    getActionBar().setDisplayHomeAsUpEnabled(true);
    tweetButton = (Button) findViewById(R.id.tweetButton);
    selectContactButton = (Button) findViewById(R.id.selectContactButton);
    emailTweetButton = (Button) findViewById(R.id.emailTweetButton);

    Bundle test = getIntent().getExtras();

    // user has not picked a tweet from the list they hit + to start new tweet
    if (test == null)
    {
    tweetButton.setOnClickListener(this);

    tweetText = (EditText) findViewById(R.id.tweetText);
    tweetText.addTextChangedListener(this);
    charCount = (TextView) findViewById(R.id.charCount);

    tweet = new Tweet();

    tweetDateTime = (TextView) findViewById(R.id.tweetDateTime);
    tweetDateTime.setText(tweet.getDateString());

    MyTweetApp app = (MyTweetApp) getApplication();
    portfolio = app.portfolio;
    }
    else
    {
    MyTweetApp app = (MyTweetApp) getApplication();
    portfolio = app.portfolio;

    UUID tweId = (UUID) getIntent().getExtras().getSerializable("TWEET_ID");

    tweetText = (EditText) findViewById(R.id.tweetText);

    tweet = portfolio.getTweet(tweId);
    if (tweet != null)
    {
    updateControls(tweet);
    }
    }
    }
  public void updateControls(Tweet tweet)
  {
    tweetText.setText(tweet.tweetText);
    selectContactButton.setOnClickListener(this);
    emailTweetButton.setOnClickListener(this);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.mytweet, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    switch (item.getItemId())
    {
      case android.R.id.home:
        navigateUp(this);
        return true;
    }

    return super.onOptionsItemSelected(item);

  }
  @Override
  public void onClick(View v)
  {
    switch (v.getId())
    {

      case R.id.tweetButton:
        String found = tweet.getTweetText();
        if (found == null)
        {
          Toast toast1 = Toast.makeText(this, "Nothing to tweet", Toast.LENGTH_SHORT);
          toast1.show();
          break;
        }
        else
        {
          tweet.getTweetText();
          tweets.add(tweet);
          portfolio.addTweet(tweet);
          startActivityWithDataForResult(this, TweetListActivity.class, "myTweet_ID", 0, 0);
          break;
        }

      case R.id.selectContactButton:
        selectContact(this, REQUEST_CONTACT);
        break;

      case R.id.emailTweetButton :
        sendEmail(this, " ", getString(R.string.tweet_email_subject), tweet.getTweetEmail(this));
        break;
    }
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    switch (requestCode)
    {
      case REQUEST_CONTACT:
        String email = getEmail(this, data);
        tweet.selectContactButton = email;
        selectContactButton.setText(email);
        break;
    }
  }

  @Override
  public void beforeTextChanged(CharSequence s, int start, int count, int after)
  {
  }

  @Override
  public void onTextChanged(CharSequence s, int start, int before, int count)
  {
    int x = 140 - s.length();
    charCount.setText("" + x);
    if (x == 0)
    {
      Toast toast = Toast.makeText(this, "Character Limit Reached", Toast.LENGTH_SHORT);
      toast.show();
    }
  }


  @Override
  public void afterTextChanged(Editable editable)
  {
    tweet.setTweetText(editable.toString());
  }

  public void onPause()
  {
    super.onPause();
    portfolio.saveTweets();
  }
}