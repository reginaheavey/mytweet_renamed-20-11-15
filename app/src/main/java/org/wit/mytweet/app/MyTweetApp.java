package org.wit.mytweet.app;

import android.app.Application;
import org.wit.mytweet.models.Portfolio;
import org.wit.mytweet.models.PortfolioSerializer;

import static org.wit.helpers.LogHelpers.info;


public class MyTweetApp extends Application
{
  public Portfolio portfolio;
  private static final String FILENAME="portfolio.json";

  @Override
  public void onCreate()
  {
    super.onCreate();
    PortfolioSerializer serializer = new PortfolioSerializer(this, FILENAME);
    portfolio = new Portfolio(serializer);
    info(this, "My Tweet app launched");
  }
}