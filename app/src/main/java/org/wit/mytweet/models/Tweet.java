package org.wit.mytweet.models;

import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;
import org.wit.mytweet.R;
import android.content.Context;

public class Tweet
{
  public UUID id;
  // represents text entered in tweet
  public String tweetText;
  public Date date;
  public String selectContactButton;

  private static final String JSON_ID                   = "id"            ;
  private static final String JSON_TWEETTEXT            = "tweetText"   ;
  private static final String JSON_TWEETDATETIME        = "tweetDateTime"          ;
  private static final String JSON_SELECTCONTACTBUTTON  = "selectContactButton"          ;

  public Tweet()
  {
    id = UUID.randomUUID();
    date = new Date();
  }

  //a new constructor to load a Tweet object from JSON:
  public Tweet(JSONObject json) throws JSONException
  {
    id            = UUID.fromString(json.getString(JSON_ID));
    tweetText     = json.getString(JSON_TWEETTEXT);
    date          = new Date(json.getLong(JSON_TWEETDATETIME));
    selectContactButton = json.getString(JSON_SELECTCONTACTBUTTON);
  }

  //method to save an object to JSON:
  public JSONObject toJSON() throws JSONException
  {
    JSONObject json = new JSONObject();
    json.put(JSON_ID            , id.toString());
    json.put(JSON_TWEETTEXT     , tweetText);
    json.put(JSON_TWEETDATETIME , date.getTime());
    json.put(JSON_SELECTCONTACTBUTTON, selectContactButton);
    return json;
  }

  public void setTweetText(String tweetText)
  {
    this.tweetText = tweetText;
  }

  public String getTweetText()
  {
    return tweetText;
  }

  public String getDateString() {
    return DateFormat.getDateTimeInstance().format(date);
  }

  //method to generate the contents of the email:
  public String getTweetEmail(Context context)
  {
    String report =  tweetText;
    return report;
  }

}